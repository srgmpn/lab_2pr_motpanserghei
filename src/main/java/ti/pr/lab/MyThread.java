package ti.pr.lab;

/**
 * Created by administrator on 3/8/15.
 */
public class MyThread extends Thread {

    private int id;
    EventsMethod eventsMethod;

    public MyThread(int id, EventsMethod eventsMethod){
        this.id = id;
        this.eventsMethod = eventsMethod;
    }

    @Override
    public void run() {

        eventsMethod.eventsFunctions(id);

    }
}

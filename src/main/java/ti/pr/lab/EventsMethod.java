package ti.pr.lab;

import java.util.ArrayList;


/**
 * Created by administrator on 2/22/15.
 */
public class EventsMethod {

    boolean [] events;
    ArrayList<Integer> [] levels;
    int [] markers;
    int level = 0;

    public EventsMethod(int [] markers, ArrayList<Integer> [] levels, int nrEvents){

        events = new boolean[nrEvents+1];
        this.markers = markers;
        this.levels = levels;

        setValues(0);
    }

    /**
     *  apeleaza functiile specificate
     *  cu id indicat
     * @param id - event id
     */
    public synchronized void eventsFunctions(int id){
        threadWait(id);
        showMsg(id);
        threadSleep();
        setValues(markers[id-1]+1);
        notifyAll();
    }

    /**
     * verifica conditia de aplelare a functiei setEvents
     * @param lev - nivel event
     */
    private void setValues(int lev) {
        level++;

        if (lev == 0) {
            setEvents(lev);
        } else if ((level == levels[lev - 1].size()) && (lev <= (levels.length - 1))) {
            setEvents(lev);
        }
    }

    /**
     *  seteaza pe true toate eventurile
     *  din tabloul events de la nivelul dat
     * @param lev - nivelul eventului
     */
    private void setEvents(int lev){
        for (int i = 0; i < levels[lev].size(); i++){
            events[levels[lev].get(i)] = true;
        }
        level = 0;
    }

    /**
     *  pune pe asteptare firul cit
     *  conditia este falsa
     * @param id - id event
     */
    private void threadWait(int id){
        while (!events[id])
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    /**
     * sleep thread
     */
    private void threadSleep(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * afiseaza mesaj eveniment
     * @param id - nr event
     */
    private void showMsg(int id){
        final String TAB = "\t\t\t";
        String tabs = "";
        int k = markers[id-1];

        //setarea nr de tabs in dependenta nivel
        while (k-- > 0)
            tabs += TAB;

        System.out.println(tabs + "Event"+ id +"->(" + markers[id-1] + ")");
    }
}

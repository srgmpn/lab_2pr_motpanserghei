package ti.pr.lab;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


/**
 * Created by administrator on 2/23/15.
 */
public class Main {
    public static void main(String[] args) {

        //citire date
        Scanner scanner = new Scanner(System.in);

        System.out.print("Dati nr de evenimente : ");
        int nrEvents = scanner.nextInt();

        System.out.print("Dati nr de nivele : ");
        int nrLevels = scanner.nextInt();

        ArrayList<Integer> [] events = new ArrayList[nrLevels];
        int [] markers = new int[nrEvents+1];

        for (int i = 0; i < nrLevels; ++i) {

            events[i] = new ArrayList<Integer>();

            while (true) {
                System.out.print("Nivelul " + (i + 1) + " dati event (next level -1):");

                int nr = scanner.nextInt();
                if (nr == -1)
                    break;
                events[i].add(nr);
                markers[nr-1] = i;
            }
        }

        EventsMethod eventsMethod = new EventsMethod(markers, events, nrEvents);

        System.out.println("\n__ Rezultatul executiei schemei __\n");

        ExecutorService service = Executors.newFixedThreadPool(10);
        for (int i = 1; i <= nrEvents; ++i){

            service.submit(new MyThread(i, eventsMethod));
        }

        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
